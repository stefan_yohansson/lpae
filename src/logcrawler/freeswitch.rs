use std::fs;
use std::path::Path;
use std::fs::File;
use std::io::{BufRead, BufReader, Write};
use std::process::Command;
use config::loader::{ConfigLoadError};
use config::Config;
use config::FreeswitchConfig;
use regex::Regex;


pub fn locate(uuid: String, config: Result<Config, ConfigLoadError>) {
    match config {
        Ok(conf) => {
            locate_by_uuid(uuid, conf.fs_conf);
        },
        Err(conf_err) => {
            panic!("failed to execute process: {:?}", conf_err);
        },
    }
}

fn locate_logfile(uuid: &String, extract_logfile: &mut fs::File, file: fs::DirEntry, filename: &str) {
    let log = File::open(file.path()).unwrap();
    let buffered_file = BufReader::new(log);

    for line in buffered_file.lines() {
        match line {
            Ok(l) => {
                if l.find(uuid) != None {
                    let line_ = format!("{} => {} \n", filename, l);
                    println!("{:?} => {:?}", filename, l);
                    extract_logfile.write(line_.as_ref()).unwrap();
                }
            },
            Err(..) => {}
        };
    }
}

fn locate_by_uuid(uuid: String, config: FreeswitchConfig) {
    // check and create output folder and file
    let folder = format!("/tmp/{}", uuid);

    match Command::new("mkdir").arg(&folder).output() {
        Ok(output) => output,
        Err(e) => panic!("failed to execute process: {}", e),
    };

    let archive = format!("/tmp/{}/{}_FS.log", uuid, uuid);
    let fs_file = Path::new(&archive);
    let mut extract_logfile = fs::OpenOptions::new()
                .read(true)
                .write(true)
                .create(true)
                .open(&fs_file).unwrap();

    // search in log files
    let paths = fs::read_dir(&Path::new(&config.freeswitch_log_path)).unwrap();

    for path in paths {
        match path {
            Ok(file) => {
                let fp = file.path();
                let file_path = Path::new(&fp);
                let filename = file_path.file_name().unwrap();

                let re = Regex::new(&config.freeswitch_log_file_pattern).unwrap();
                if !re.is_match(filename.to_str().unwrap()) {
                    println!("skipping: {:?}", filename);
                    continue;
                }

                match file_path.extension() {
                    Some(ext) => {
                        if ext == "log" {
                            locate_logfile(&uuid, &mut extract_logfile, file, filename.to_str().unwrap());
                        }
                    },
                    _ => {

                    },
                }
            },
            Err(..) => {

            }
        }
    }
}
