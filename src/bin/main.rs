extern crate rustc_serialize;
extern crate docopt;
extern crate logcrawler;

use std::process;
use docopt::Docopt;
use logcrawler::logcrawler::freeswitch;
use logcrawler::logcrawler::auc;
use logcrawler::config::loader::ConfigLoader;
use logcrawler::config::file_loader::FileLoader;

// Write the Docopt usage string.
static USAGE: &'static str = "
Usage: lpae -i <entrypoint> [<id>]

Options:
    -i, --init  entrypoint.
";

#[derive(RustcDecodable, Debug)]
struct Args {
    arg_entrypoint: String,
    arg_id: String,
    flag_init: bool,
}

fn main() {
    let args: Args = Docopt::new(USAGE)
                            .and_then(|d| d.decode())
                            .unwrap_or_else(|e| e.exit());

    let config_file_loader = FileLoader{file_path: "lpae.toml"};
    let config = config_file_loader.load();

    match &*args.arg_entrypoint {
        "freeswitch" => {
            if args.arg_id == "" {
                println!("we need an uuid for freeswitch.");
                process::exit(1);
            }

            freeswitch::locate(args.arg_id, config);
        }
        "auc" => {
            if args.arg_id == "" {
                println!("we need an sid for auc.");
                process::exit(1);
            }

            auc::locate(args.arg_id, config);
        }
        _ => {

        }
    }

}
