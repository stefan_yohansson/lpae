use std::{io, convert};
use config::Config;
use toml;

pub trait ConfigLoader {
	fn load(&self) -> Result<Config, ConfigLoadError>;
}

#[derive(Debug)]
pub enum ConfigLoadError {
	EmptyError,
	LoadIoError(io::Error),
	ParseError(toml::ParserError),
	DecodeError(toml::DecodeError),
}

impl convert::From<io::Error> for ConfigLoadError {
	fn from(err: io::Error) -> ConfigLoadError {
		ConfigLoadError::LoadIoError(err)
	}
}

impl From<toml::DecodeError> for ConfigLoadError {
	fn from(err: toml::DecodeError) -> ConfigLoadError {
		ConfigLoadError::DecodeError(err)
	}
}
