pub mod loader;
pub mod file_loader;

#[derive(Debug)]
#[derive(RustcDecodable)]
pub struct Config {
    pub fs_conf: FreeswitchConfig,
    pub auc_conf: AucConfig,
}

#[derive(Debug)]
#[derive(RustcDecodable)]
pub struct FreeswitchConfig {
    pub freeswitch_log_path: String,
    pub freeswitch_log_file_pattern: String,
    pub freeswitch_record_path: String,
}

#[derive(Debug)]
#[derive(RustcDecodable)]
pub struct AucConfig {
    pub auc_log_path: String,
    pub auc_log_file_pattern: String
}
